[toc]
这是一条笔记内容
# 标题1
## 标题2
### 标题3
#### 标题4


 - 选项1
 - 选项2
 - 选项3

 1. 有序1
 2. 有序2
 3. 有序3

 ```cpp{.line-numbers}
 #include<iostream>
 int main()
 {
    std::cout << "hello " << std::endl;
 }
 ```
 
 ```bash{.line-numbers}
 read var
 var ${read}
 ```

 ```python{.line-numbers}
def func(num:int) -> None:
    return num**3
print(func(100))
 ```

![作业2](%E4%BD%9C%E4%B8%9A2.PNG)
[python](https://python.org)

- [x] 
- [x] 
- [x] 
- [ ] 
- [x] 



:smile:
:fa-car:
:cry:

$f(x) = sin(x) + 12$
$$\sum_{n=1}^{100}$$

<!-- ```mermaid
graph LR:
    A -> B;
    B -> C;
    C -> A;
``` -->

```dot
digraph G {
    A -> B
    B -> C
    B -> D
}
```

```dot {engine="circo"}
digraph G{
    A -> B
    B -> C
    B -> D
}
```

@import "main.py"


