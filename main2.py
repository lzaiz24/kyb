import requests
import json
import pprint
from bs4 import BeautifulSoup
if __name__ == "__main__":
    r = requests.get("https://yz.njfu.edu.cn/sszs/")
    r.encoding = "utf-8"  # 设置编码格式
    soup = BeautifulSoup(r.text, "html.parser")
    # print(r.text)
    for target in soup.find_all('script'):
        if "dataList=" in target.get_text():
            # print(target.get_text().find("dataList="))
            # print(target.get_text()[108:])
            # print(target.get_text().find("var pagesData="))
            start_index = target.get_text().find("dataList=")+len("dataList=")
            end_index = target.get_text().find("var pagesData=")
            info = json.loads(target.get_text()[
                              start_index:end_index].rstrip().rstrip(";"))
        
            for row in info:
                for result in row['infolist']:
                    print(result['title'], result['url'], result['daytime'])
